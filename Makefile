DEVICE := osborn

MODEM_IMAGE := firmware-update/NON-HLOS.bin
XBL_ELF := firmware-update/xbl.elf
ABL_ELF := firmware-update/abl.elf

TIMESTAMP := $(shell strings $(MODEM_IMAGE) | sed -n 's/.*"Time_Stamp": "\([^"]*\)"/\1/p')
VERSION := $(shell echo $(TIMESTAMP) | sed 's/[ :-]*//g')

HASH_XBL := $(shell openssl dgst -r -sha1 $(XBL_ELF) | cut -d ' ' -f 1)
HASH_ABL := $(shell openssl dgst -r -sha1 $(ABL_ELF) | cut -d ' ' -f 1)

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: assert inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

$(TARGET): META-INF firmware-update
	zip -r9 $@ $^

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Assert
# ==========
.PHONY: assert
assert: $(XBL_ELF) $(ABL_ELF)
ifneq ($(HASH_XBL), 90673538862b65268403c13d45b977db181703d6)
	$(error SHA-1 of xbl.elf mismatch)
endif
ifneq ($(HASH_ABL), e5354bf5efcb0457c5ad20ee35e88242f27af843)
	$(error SHA-1 of abl.elf mismatch)
endif

# Inspect
# ==========

.PHONY: inspect
inspect: $(MODEM_IMAGE)
	@echo Target: $(TARGET)
	@echo Timestamp: $(TIMESTAMP)
